import statistics
import csv
import scipy.stats
from tabulate import tabulate

# save the dataset in a list of dictionaries
dataset = [*csv.DictReader(open('dataset/DataSaiyans-Cleaned-AirBnB.csv'))]


# provides the mean, median, mode, variance, and standard deviation of a specified column name from the sata set.
def describe_int(column_name):
    data = []
    result = []
    for row in dataset:
        if row[column_name] != "Unknown":
            data.append(float(row[column_name]))
    print(column_name)
    result.append(["Mean", statistics.mean(data)])
    result.append(["Median", statistics.median(data)])
    result.append(["Mode", statistics.mode(data)])
    result.append(["Standard Deviation", statistics.stdev(data)])
    result.append(["Variance", statistics.variance(data)])

    print(tabulate(result), "\n")


# saves the details of each neighborhood group in a dictionary in the form, "Name of Group": (Count, Total Price)
def get_average_price_per_neighborhood_group():
    result = dict()
    table_data = []
    for row in dataset:
        neighborhood_group = row["Neighbourhood Group"]
        if row["Price"] != "Unknown":
            if neighborhood_group in result:
                result[neighborhood_group] = (
                    result.get(neighborhood_group)[0] + 1, result.get(neighborhood_group)[1] + int(row["Price"]))
            else:
                result[neighborhood_group] = (1, int(row["Price"]))
    for key in result.keys():
        if result.get(key)[0] > 1:
            table_data.append([key, int(result.get(key)[1]) / int(result.get(key)[0])])
    print(tabulate(table_data, headers=("Neighbourhood Group", "Average Price")))


def describe_dataset():
    describe_int("Price")
    describe_int("Number of Reviews")
    describe_int("Service Fee")
    describe_int("Minimum Nights")


describe_dataset()
get_average_price_per_neighborhood_group()
